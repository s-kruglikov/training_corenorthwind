﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace CoreNorthWind.Helpers
{
	[HtmlTargetElement("a", Attributes = "northwind-id")]
	public class ImageLinkTagHelper : TagHelper
	{
		#region Private Fields
		
		private readonly IUrlHelperFactory _urlHelperFactory;
		
		#endregion

		#region Constructor
		
		public ImageLinkTagHelper(IUrlHelperFactory helperFactory)
		{
			_urlHelperFactory = helperFactory;
		}
		
		#endregion
		
		#region Properties
		
		[ViewContext]
		[HtmlAttributeNotBound]
		public ViewContext ViewContext { get; set; }
		
		public string PageAction { get; set; }

		public int? NorthwindId { get; set; }
		
		#endregion
		
		public override void Process(TagHelperContext context, TagHelperOutput output)
		{
			IUrlHelper urlHelper = _urlHelperFactory.GetUrlHelper(ViewContext);
			
			TagBuilder result = new TagBuilder("a");
			if (NorthwindId != null)
			{
				result.Attributes["href"] = urlHelper.Action(PageAction, new {categoryId = NorthwindId.Value});
				result.Attributes["target"] = "_blank";
				result.Attributes["rel"] = "noopener noreferrer";
				result.InnerHtml.Append("Open Image");
			}

			output.Content.AppendHtml(result);
		}
	}
}