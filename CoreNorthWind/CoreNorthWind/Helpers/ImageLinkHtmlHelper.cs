﻿using System.IO;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CoreNorthWind.Helpers
{
	public static class ImageLinkHtmlHelper
	{
		public static HtmlString ImageLink(this IHtmlHelper helper, int imageId, string linkText)
		{
			TagBuilder result = new TagBuilder("a");

			result.Attributes["href"] = $"/images/{imageId}";
			result.Attributes["target"] = "_blank";
			result.Attributes["rel"] = "noopener noreferrer";
			result.InnerHtml.Append(linkText);
			
			var writer = new StringWriter();
			result.WriteTo(writer, HtmlEncoder.Default);
			return new HtmlString(writer.ToString());
		}
	}
}