using System.IO;
using AutoMapper;
using CoreNorthWind.Contexts;
using CoreNorthWind.Domain.Contexts;
using CoreNorthWind.Domain.Repositories;
using CoreNorthWind.Domain.Utilities;
using CoreNorthWind.Filters;
using CoreNorthWind.Infrastructure;
using CoreNorthWind.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace CoreNorthWind
{
	public class Startup
	{
		#region Constructor
		
		public Startup(IConfiguration configuration, IWebHostEnvironment env)
		{
			Configuration = configuration;
			Env = env;
		}
		
		#endregion

		#region Properties

		private IConfiguration Configuration { get; }

		private ILogger Logger { get; set; }

		private IWebHostEnvironment Env { get; }
		
		#endregion

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{



			var builder = services.AddRazorPages();

#if DEBUG
			if (Env.IsDevelopment())
			{
				builder.AddRazorRuntimeCompilation();
			}
#endif
			services.AddControllersWithViews();

			RegisterFilters(services);
			RegisterUtilities(services);
			RegisterDbContext(services);
			RegisterIdentity(services);
			RegisterRepositories(services);

			services.AddAutoMapper(typeof(Startup));

			services.AddMvc();

			services.AddSwaggerGen(c =>
			{
				c.CustomOperationIds(e => $"{e.ActionDescriptor.RouteValues["controller"]}_{e.HttpMethod}");
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "CoreNorthWindApi", Version = "v1" });
			});
		}
		
		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
		{
			loggerFactory.AddFile(Path.Combine(Directory.GetCurrentDirectory(), "log.txt"));

			Logger = loggerFactory.CreateLogger("FileLogger");

			LoggingActionFilter._logger = Logger;

			if (Env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseImagesCaching(Logger);
			app.UseHttpsRedirection();
			
			app.UseStaticFiles();
			app.UseStaticFiles(new StaticFileOptions
			{
				FileProvider = new PhysicalFileProvider(
					Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "app")),
				RequestPath = "/spa"
			});

			app.UseRouting();

			app.UseAuthentication();
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					"images",
					pattern: "images/{categoryId?}",
					defaults: new { controller = "Categories", action = "GetImage" });

				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});

			app.UseSwagger(o => o.SerializeAsV2 = true);
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "CoreNorthWind API V1");
			});

			Logger.LogInformation("Application started at: '{0}'", Directory.GetCurrentDirectory());
		}

		#region Services Registrations
		
		private void RegisterRepositories(IServiceCollection services)
		{
			services.AddTransient<ICategoriesRepository, CategoriesRepository>();
			services.AddTransient<IProductsRepository, ProductsRepository>();
		}

		private void RegisterDbContext(IServiceCollection services)
		{
			services.AddDbContext<DataDbContext>(options =>
				options.UseSqlServer(Configuration.GetConnectionString("AzureDBContext")));
			services.AddDbContext<AccountDbContext>(options =>
				options.UseSqlServer(Configuration.GetConnectionString("AzureDBContext")));
		}

		private void RegisterIdentity(IServiceCollection services)
		{
			services.AddIdentity<IdentityUser, IdentityRole>()
				.AddEntityFrameworkStores<AccountDbContext>();
		}

		private void RegisterUtilities(IServiceCollection services)
		{
			services.AddTransient<IImagesProcessor, ImagesProcessor>();
		}
		
		private void RegisterFilters(IServiceCollection services)
		{
			services.AddTransient<LoggingActionFilter>();
		}
		
		#endregion
	}
}
