﻿using AutoMapper;
using CoreNorthWind.Domain.Models;
using CoreNorthWind.Models;

namespace CoreNorthWind.Maps
{
	public class ProductMappingProfile : Profile
	{
		public ProductMappingProfile()
		{
			CreateMap<Product, ProductAPIModel>()
				.ForMember(dest => dest.CategoryName, opt =>
				{
					opt.MapFrom(src => src.Category.CategoryName);
				});
		}
	}
}
