﻿using AutoMapper;
using CoreNorthWind.Domain.Models;
using CoreNorthWind.Models;

namespace CoreNorthWind.Maps
{
	public class CategoryMappingProfile : Profile
	{
		public CategoryMappingProfile()
		{
			CreateMap<Category, CategoryAPIModel>();
		}
	}
}
