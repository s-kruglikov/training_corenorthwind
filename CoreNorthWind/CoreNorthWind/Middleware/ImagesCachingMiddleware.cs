﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CoreNorthWind.Middleware
{
	public class ImagesCachingMiddleware
	{
		private readonly RequestDelegate _next;
		private readonly ILogger _logger;
		private readonly IConfiguration _configuration;

		public ImagesCachingMiddleware(RequestDelegate next, ILogger logger, IConfiguration configuration)
		{
			_next = next;
			_logger = logger;
			_configuration = configuration;
		}

		public async Task InvokeAsync(HttpContext context)
		{
			var request = context.Request;
			string imagesCachePath = _configuration.GetValue<string>("ImagesCachePath");
			bool imagesCacheEnabled = _configuration.GetValue<bool>("ImagesCacheEnable");
			int imagesCacheMaxCount = _configuration.GetValue<int>("ImagesCacheCount");

			if (imagesCacheEnabled &&
			    request.Path.HasValue &&
			    request.Path.Value.Contains("/images/"))
			{
				var imageId = request.Path.Value.Split("/").Last();

				var filePath = $"{imagesCachePath}\\{imageId}.bmp";

				if (File.Exists(filePath))
				{
					context.Response.ContentType = "image/bmp";
					using (var fs = new FileStream(filePath, FileMode.Open))
					{
						await fs.CopyToAsync(context.Response.Body);
					}
				}
				else
				{
					Stream originalBody = context.Response.Body;

					try
					{
						await using var memStream = new MemoryStream();
						context.Response.Body = memStream;

						await _next(context);



						if (Directory.GetFiles(imagesCachePath).Length < imagesCacheMaxCount)
						{

							memStream.Position = 0;

							await using (var fileStream = File.Create(filePath))
							{
								while (true)
								{
									var read = memStream.ReadByte();

									if (read > -1)
									{
										fileStream.WriteByte(Convert.ToByte(read));
									}
									else
									{
										fileStream.Flush();
										break;
									}

								}
							}
						}

						memStream.Position = 0;
						await memStream.CopyToAsync(originalBody);
					}
					finally
					{
						context.Response.Body = originalBody;
					}
				}
			}
			else
			{
				// Call the next delegate/middleware in the pipeline
				await _next(context);
			}
		}
	}
}
