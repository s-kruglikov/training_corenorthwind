﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;

namespace CoreNorthWind.Middleware
{
	public static class ImagesCachingMiddlewareExtension
	{
		public static IApplicationBuilder UseImagesCaching(
			this IApplicationBuilder builder, ILogger logger)
		{
			return builder.UseMiddleware<ImagesCachingMiddleware>(logger);
		}
	}
}
