﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CoreNorthWind.Filters
{
	public class LoggingActionFilter : ActionFilterAttribute
	{
		private readonly IConfiguration _configuration;
		//private readonly ILogger _logger;
		private Stopwatch _timer;

		public bool Enable { get; set; }

		public static ILogger _logger { get; set; }

		public LoggingActionFilter(IConfiguration configuration)
		{
			_configuration = configuration;
			//_logger = logger;
		}

		public override void OnActionExecuting(ActionExecutingContext context)
		{
			

			if (_configuration.GetValue<bool>("LoggingAction"))
			{
				var actionName = context.ActionDescriptor.DisplayName.ToString();

				//context.Controller.ControllerContext.ActionDescriptor.ControllerName
				_logger.Log(LogLevel.Information, $"Begin executing action '{actionName}'");
				_timer = Stopwatch.StartNew();
			}

		}

		public override void OnActionExecuted(ActionExecutedContext context)
		{
			if (_configuration.GetValue<bool>("LoggingAction"))
			{
				var actionName = context.ActionDescriptor.DisplayName.ToString();

				_timer.Stop();
				string result = $"Elapsed time: {_timer.Elapsed.TotalMilliseconds} ms";

				_logger.Log(LogLevel.Information, $"End executing action. '{actionName}'" + result);
			}
		}
	}
}
