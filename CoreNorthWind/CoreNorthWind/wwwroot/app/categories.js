const categoriesUri = '../api/categories';

function getCategories(){
    $.getJSON(categoriesUri)
        .done(function (data) {
            // On success, 'data' contains a list of categories.
            $.each(data, function (key, item) {
                // Add a list item for the categories.
                $('<li>', { text: formatCategoryItem(item) }).appendTo($('#categories'));
            });
        });
}

function formatCategoryItem(item) {
    return `${item.categoryName} - ${item.description}`;
}