﻿const productsUri = '../api/products';

function getProducts(){
    $.getJSON(productsUri)
        .done(function (data) {
            // On success, 'data' contains a list of products.
            
            $('<thead class="thead-dark"><tr>' +
                '<td>Product Name</td>'+
                '<td>Category Name</td>'+
                '<td>Unit Price</td>'+
                '<td>Units In Stock</td>'+
                '<td>Units On Order</td>'+
                '</tr></thead>').appendTo($('#products'));
            $('<tbody>').appendTo($('#products'));
            
            $.each(data, function (key, item) {
                let element = formatProductItem(item);
                $(element.appendTo($('#products')));
            });

            $('</tbody>').appendTo($('#products'));
        });
}

function formatProductItem(item) {
    let formattedElement = `<tr><td>${item.productName}</td>`+
        `<td>${item.categoryName}</td>`+
        `<td>${item.unitPrice}</td>` +
        `<td>${item.unitsInStock}</td>` +
        `<td>${item.unitsOnOrder}</td></tr>`;
    return $(formattedElement);
}