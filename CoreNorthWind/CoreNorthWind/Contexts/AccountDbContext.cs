﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CoreNorthWind.Contexts
{
	public class AccountDbContext : IdentityDbContext
	{
		public AccountDbContext(DbContextOptions<AccountDbContext> options)
			: base(options)
		{
		}
	}
}
