﻿using CoreNorthWind.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CoreNorthWind.Components
{
	public class BreadcrumbsViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke(string delimiter = " / ")
		{
			var model = new BreadcrumbsViewModel
			{
				Delimiter = delimiter,

				// Add Home page as default to the breadcrumbs collection.
				Items = new List<BreadcrumbItemViewModel> { new BreadcrumbItemViewModel
				{
					Action = "Index",
					Controller = "Home",
					LinkText = "Home"
				} }
			};

			var controller = RouteData.Values["controller"].ToString();
			var action = RouteData.Values["action"].ToString();

			model.Items.Add(new BreadcrumbItemViewModel
			{
				Action = "Index",
				Controller = controller,
				LinkText = controller
			});

			if (action == "Add" || action == "Edit")
			{
				model.Items.Add(new BreadcrumbItemViewModel
				{
					Action = action,
					Controller = controller,
					LinkText = "Create New"
				});
			}

			return View("Default", model);
		}
	}
}
