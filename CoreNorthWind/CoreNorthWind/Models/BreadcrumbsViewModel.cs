﻿using System.Collections.Generic;

namespace CoreNorthWind.Models
{
	public class BreadcrumbsViewModel
	{
		public string Delimiter { get; set; }

		public List<BreadcrumbItemViewModel> Items { get; set; }
	}
}
