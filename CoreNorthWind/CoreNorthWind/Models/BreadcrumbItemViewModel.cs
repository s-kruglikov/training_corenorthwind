﻿namespace CoreNorthWind.Models
{
	public class BreadcrumbItemViewModel
	{
		public string Controller { get; set; }

		public string Action { get; set; }

		public string LinkText { get; set; }
	}
}
