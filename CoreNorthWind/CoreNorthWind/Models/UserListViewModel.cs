﻿using System.Collections.Generic;

namespace CoreNorthWind.Models
{
	public class UserListViewModel
	{
		public IList<UserViewModel> Users { get; set; }
	}
}
