﻿using Newtonsoft.Json;

namespace CoreNorthWind.Models
{
	public class CategoryAPIModel
	{
		[JsonProperty(PropertyName = "Id")]
		public string CategoryID { get; set; }

		[JsonProperty(PropertyName = "Name")]
		public string CategoryName { get; set; }

		[JsonProperty(PropertyName = "Description")]
		public string Description { get; set; }
	}
}
