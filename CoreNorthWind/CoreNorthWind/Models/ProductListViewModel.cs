﻿using System.Collections.Generic;
using CoreNorthWind.Domain.Models;

namespace CoreNorthWind.Models
{
	public class ProductListViewModel
	{
		public IEnumerable<Product> Products { get; set; }

		public PagingInfoViewModel PagingInfo { get; set; }

	}
}
