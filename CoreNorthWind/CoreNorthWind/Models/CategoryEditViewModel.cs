﻿using CoreNorthWind.Domain.Models;
using Microsoft.AspNetCore.Http;

namespace CoreNorthWind.Models
{
	public class CategoryEditViewModel
	{
		public Category Category { get; set; }

		public IFormFile ImageUpdate { get; set; }
	}
}
