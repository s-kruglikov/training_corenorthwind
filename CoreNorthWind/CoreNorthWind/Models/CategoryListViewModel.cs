﻿using System.Collections.Generic;
using CoreNorthWind.Domain.Models;

namespace CoreNorthWind.Models
{
	public class CategoryListViewModel : ListViewModel
	{
		public IEnumerable<Category> Categories { get; set; }

		public int Rows { get; set; }

		public int Cols { get; set; }
	}
}
