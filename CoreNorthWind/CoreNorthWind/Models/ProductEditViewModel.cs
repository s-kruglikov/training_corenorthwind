﻿using System.Collections.Generic;
using CoreNorthWind.Domain.Models;

namespace CoreNorthWind.Models
{
	public class ProductEditViewModel
	{
		public Product Product { get; set; }

		public IEnumerable<Category> Categories { get; set; }
	}
}
