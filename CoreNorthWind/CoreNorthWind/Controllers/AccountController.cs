﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using CoreNorthWind.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace CoreNorthWind.Controllers
{
	public class AccountController : Controller
	{
		private readonly UserManager<IdentityUser> _userManager;
		private readonly SignInManager<IdentityUser> _signInManager;
		private readonly ILogger<AccountController> _logger;

		public AccountController(
			UserManager<IdentityUser> userManager,
			SignInManager<IdentityUser> signInManager,
			ILogger<AccountController> logger)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_logger = logger;
		}

		[HttpGet]
		public IActionResult LoginWithAAD()
		{
			string tenant = "b41b72d0-4e9f-4c26-8a69-f949f367c91d";

			var uriFormat = $"https://login.microsoftonline.com/{tenant}/oauth2/authorize?client_id=0f31efb9-5eb6-4be5-8bf8-b19ab16e0ad4&response_type=id_token&redirect_uri=https://jwt.ms&response_mode=form_post&scope=openid&state=12345&nonce=7362CAEA-9CA5-4B43-9BA3-34D7C303EBA7";

			// prepare msgraph authentication request
			var client = new RestClient("https://login.microsoftonline.com");
			var request = new RestRequest($"{tenant}/oauth2/authorize")
			{
				AlwaysMultipartFormData = true
			};

			request.AddHeader("Cache-Control", "no-cache");
			request.AddParameter("client_id", "0f31efb9-5eb6-4be5-8bf8-b19ab16e0ad4");
			request.AddParameter("response_type", "id_token");
			request.AddParameter("redirect_uri", @"https://skcorenorthwind.azurewebsites.net/Account/LoginWithAAD");
			request.AddParameter("response_mode", "form_post");
			request.AddParameter("scope", "openid");
			request.AddParameter("state", "12345");
			request.AddParameter("nonce", "7362CAEA-9CA5-4B43-9BA3-34D7C303EBA7");

			// send and extract token
			var result = client.Get(request);

			return Redirect(result.ResponseUri.ToString());
		}

		[HttpPost]
		public async Task<IActionResult> LoginWithAAD(string body)
		{
			var idToken = Request.Form["id_token"].ToString();

			var tokenHandler = new JwtSecurityTokenHandler();
			var securityToken = tokenHandler.ReadToken(idToken) as JwtSecurityToken;

			var uniqueName = securityToken.Claims.FirstOrDefault(c => c.Type == "unique_name").Value;
			var firstName = securityToken.Claims.FirstOrDefault(c => c.Type == "given_name").Value;
			var lastName = securityToken.Claims.FirstOrDefault(c => c.Type == "family_name").Value;

			await _signInManager.SignInAsync(new IdentityUser()
			{
				UserName = firstName + " " + lastName,
				Email = uniqueName
			}, isPersistent: false);

			return RedirectToAction("Index", "Home");
		}

		[HttpGet]
		public async Task<IActionResult> Register(string returnUrl = null)
		{
			var registerViewModel = new RegisterViewModel()
			{
				ReturnUrl = returnUrl,
				ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList()
			};
			return View(registerViewModel);
		}

		[HttpPost]
		public async Task<IActionResult> Register(RegisterViewModel registerViewModel, string returnUrl = null)
		{
			returnUrl ??= Url.Content("~/");
			var externalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

			if (ModelState.IsValid)
			{
				var user = new IdentityUser { UserName = registerViewModel.Input.Email, Email = registerViewModel.Input.Email };
				var result = await _userManager.CreateAsync(user, registerViewModel.Input.Password);
				if (result.Succeeded)
				{
					_logger.LogInformation("User created a new account with password.");

					await _signInManager.SignInAsync(user, isPersistent: false);
					return LocalRedirect(returnUrl);
				}
				foreach (var error in result.Errors)
				{
					ModelState.AddModelError(string.Empty, error.Description);
				}
			}

			// If we got this far, something failed, redisplay form
			return View(registerViewModel);
		}

		[HttpGet]
		public async Task<IActionResult> Login(string returnUrl = null)
		{
			returnUrl ??= Url.Content("~/");

			// Clear the existing external cookie to ensure a clean login process
			await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
			var loginViewModel = new LoginViewModel()
			{
				ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList(),
				ReturnUrl = returnUrl
			};

			return View(loginViewModel);
		}

		[HttpPost]
		public async Task<IActionResult> Login(LoginViewModel loginViewModel, string returnUrl = null)
		{
			returnUrl ??= Url.Content("~/");

			if (ModelState.IsValid)
			{
				// This doesn't count login failures towards account lockout
				// To enable password failures to trigger account lockout, set lockoutOnFailure: true
				var result = await _signInManager.PasswordSignInAsync(loginViewModel.Input.Email, loginViewModel.Input.Password, loginViewModel.Input.RememberMe, lockoutOnFailure: false);
				if (result.Succeeded)
				{
					_logger.LogInformation("User logged in.");
					return LocalRedirect(returnUrl);
				}
				if (result.RequiresTwoFactor)
				{
					return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, RememberMe = loginViewModel.Input.RememberMe });
				}
				if (result.IsLockedOut)
				{
					_logger.LogWarning("User account locked out.");
					return RedirectToPage("./Lockout");
				}
				else
				{
					ModelState.AddModelError(string.Empty, "Invalid login attempt.");
					return View(loginViewModel);
				}
			}

			// If we got this far, something failed, redisplay form
			return View(loginViewModel);
		}

		[HttpGet]
		public async Task<IActionResult> Logout()
		{
			await _signInManager.SignOutAsync();
			return RedirectToAction("Index", "Home");
		}
	}
}