﻿using CoreNorthWind.Domain.Repositories;
using CoreNorthWind.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;
using CoreNorthWind.Domain.Models;
using Microsoft.AspNetCore.Http;
using CoreNorthWind.Filters;
using Microsoft.AspNetCore.Authorization;

namespace CoreNorthWind.Controllers
{
	[Authorize]
	[TypeFilter(typeof(LoggingActionFilter))]
	public class CategoriesController : Controller
	{
		#region Private Fields

		private readonly ICategoriesRepository _categoriesRepository;

		#endregion

		#region Constructor

		public CategoriesController(ICategoriesRepository categoriesRepository)
		{
			_categoriesRepository = categoriesRepository;
		}

		#endregion

		public ViewResult Index(int categoryPage = 1)
		{
			var categoriesList = _categoriesRepository.GetCategories().ToList();

			var categoryListViewModel = new CategoryListViewModel
			{
				Categories = categoriesList
			};

			return View(categoryListViewModel);
		}

		public FileResult GetImage(int categoryId)
		{
			var image = _categoriesRepository.GetPicture(categoryId);

			return File(image, "image/bmp");
		}

		[HttpGet]
		public IActionResult Edit(int id)
		{
			ViewData["Title"] = "Edit Category";

			var category = _categoriesRepository.GetCategories()
				.FirstOrDefault(c => c.CategoryID == id);

			if (category == null)
			{
				return NotFound();
			}

			// We need to get actial Picture for category since it is stored with offset.
			if (category.CategoryID != null)
				category.Picture = _categoriesRepository.GetPicture(category.CategoryID.Value);

			// Create view model
			var viewModel = new CategoryEditViewModel
			{
				Category = category
			};

			return View("Edit", viewModel);
		}

		[HttpPost]

		public IActionResult Edit(CategoryEditViewModel categoryEditViewModel)
		{
			if (ModelState.IsValid &&
				(categoryEditViewModel.ImageUpdate != null || categoryEditViewModel.Category.Picture != null))

			{
				if (categoryEditViewModel.ImageUpdate != null)
				{
					byte[] imageData = null;

					using (var binaryReader = new BinaryReader(categoryEditViewModel.ImageUpdate.OpenReadStream()))
					{
						imageData = binaryReader.ReadBytes((int)categoryEditViewModel.ImageUpdate.Length);
					}

					categoryEditViewModel.Category.Picture = imageData;
				}

				_categoriesRepository.SaveCategory(categoryEditViewModel.Category);

				return RedirectToAction("Index");
			}

			ModelState.AddModelError("CategoryImage", "Please add category image");

			return View("Edit", categoryEditViewModel);
		}

		[HttpGet]
		public IActionResult Add()
		{
			ViewData["Title"] = "Add Category";

			return View("Edit", new CategoryEditViewModel());
		}

		public IActionResult Delete(int id)
		{
			var category = _categoriesRepository.GetCategories().FirstOrDefault(c => c.CategoryID == id);

			if (category != null)
			{
				_categoriesRepository.DeleteCategory(category);
			}

			return RedirectToAction("Index");
		}

		public IActionResult ThrowError()
		{
			throw new ApplicationException("Exception generated from the Categories page.");
		}
	}
}