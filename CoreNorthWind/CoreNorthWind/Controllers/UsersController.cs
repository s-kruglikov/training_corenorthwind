﻿using CoreNorthWind.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreNorthWind.Controllers
{
	[Authorize(Roles = "admin")]
	public class UsersController : Controller
	{
		private readonly UserManager<IdentityUser> _userManager;
		private readonly RoleManager<IdentityRole> _roleManager;

		public UsersController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
		{
			_userManager = userManager;
			_roleManager = roleManager;
		}

		public async Task<IActionResult> Index()
		{
			var viewModel = new UserListViewModel()
			{
				Users = new List<UserViewModel>()
			};

			foreach (var user in _userManager.Users.ToList())
			{
				var roles = await _userManager.GetRolesAsync(user);

				viewModel.Users.Add(new UserViewModel()
				{
					Id = user.Id,
					Email = user.Email,
					Roles = roles
				});
			}
			return View(viewModel);
		}

		public async Task<IActionResult> Edit(string userId)
		{
			IdentityUser user = await _userManager.FindByIdAsync(userId);
			if (user != null)
			{
				var userRoles = await _userManager.GetRolesAsync(user);
				var allRoles = _roleManager.Roles.ToList();
				UserEditViewModel model = new UserEditViewModel
				{
					UserId = user.Id,
					UserEmail = user.Email,
					UserRoles = userRoles,
					AllRoles = allRoles
				};
				return View(model);
			}

			return NotFound();
		}

		[HttpPost]
		public async Task<IActionResult> Edit(string userId, List<string> roles)
		{
			IdentityUser user = await _userManager.FindByIdAsync(userId);
			if (user != null)
			{
				var userRoles = await _userManager.GetRolesAsync(user);

				var allRoles = _roleManager.Roles.ToList();

				var addedRoles = roles.Except(userRoles);

				var removedRoles = userRoles.Except(roles);

				await _userManager.AddToRolesAsync(user, addedRoles);

				await _userManager.RemoveFromRolesAsync(user, removedRoles);

				return RedirectToAction("Index");
			}

			return NotFound();
		}
	}
}