﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CoreNorthWind.Domain.Models;
using CoreNorthWind.Domain.Repositories;
using CoreNorthWind.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoreNorthWind.Controllers
{
	[Route("api/products")]
	[ApiController]
	public class ProductsAPIController : ControllerBase
	{
		#region Private Fields

		private readonly IProductsRepository _productsRepository;
		private readonly IMapper _mapper;

		#endregion

		#region Constructor

		public ProductsAPIController(IMapper mapper, IProductsRepository productsRepository)
		{
			_mapper = mapper;
			_productsRepository = productsRepository;
		}

		#endregion

		// GET: api/products
		[HttpGet]
		public IEnumerable<ProductAPIModel> Get()
		{
			var productsList = _productsRepository.GetProducts().ToList();

			var apiModels = _mapper.Map<List<Product>, List<ProductAPIModel>>(productsList);

			return apiModels;
		}

		// GET: api/products/5
		[HttpGet("{id}", Name = "Get")]
		public ProductAPIModel Get(int id)
		{
			var product = _productsRepository.GetProducts().FirstOrDefault(p => p.ProductID == id);
			var productAPIModel = new ProductAPIModel();
			_mapper.Map(product, productAPIModel);
			
			return productAPIModel;
		}

		// POST: api/products
		[HttpPost]
		public void Post([FromBody] string value)
		{
			var productObj = JsonConvert.DeserializeObject(value, typeof(Product));

			if (productObj is Product product)
			{
				_productsRepository.UpdateProduct(product);
			}
		}

		// PUT: api/products
		[HttpPut]
		public void Put([FromBody] string value)
		{
			var productObj = JsonConvert.DeserializeObject(value, typeof(Product));

			if (productObj is Product product)
			{
				_productsRepository.AddProduct(product);
			}
		}

		// DELETE: api/products/5
		[HttpDelete("{id}")]
		public void Delete(int id)
		{
			_productsRepository.DeleteProductById(id);
		}
	}
}
