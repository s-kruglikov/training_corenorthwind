﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CoreNorthWind.Models;
using Microsoft.AspNetCore.Diagnostics;

namespace CoreNorthWind.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;

		public HomeController(ILogger<HomeController> logger)
		{
			_logger = logger;
		}

		public IActionResult Index()
		{
			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			var error = this.HttpContext.Features.Get<IExceptionHandlerFeature>().Error;
			var requestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;

			_logger.LogError($"Exception occured. Request ID: '{requestId}'. Message: {error.Message}");

			return View(new ErrorViewModel { RequestId = requestId });
		}
	}
}
