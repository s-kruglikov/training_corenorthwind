﻿using CoreNorthWind.Domain.Models;
using CoreNorthWind.Domain.Repositories;
using CoreNorthWind.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

namespace CoreNorthWind.Controllers
{
	[Authorize]
	public class ProductsController : Controller
	{
		private readonly IProductsRepository _productsRepository;
		private readonly ICategoriesRepository _categoriesRepository;
		private readonly ILogger<ProductsController> _logger;
		private readonly int _pageSize;

		private List<Product> _productsList = new List<Product>
		{
			new Product { ProductName = "product 1" },
			new Product { ProductName = "product 2" },
			new Product { ProductName = "product 3" },
			new Product { ProductName = "product 4" }
		};

		public ProductsController(IProductsRepository productsRepository,
			IConfiguration configuration,
			ILogger<ProductsController> logger, ICategoriesRepository categoriesRepository)
		{
			_productsRepository = productsRepository;
			_logger = logger;
			_categoriesRepository = categoriesRepository;
			_pageSize = configuration.GetValue<int>("ProductsPerPage");

			_logger.LogInformation("Products per page value retrieved from configuration: {0}", _pageSize);
		}

		public IActionResult Index(int productPage = 1)
		{
			var productsList = _productsRepository.GetProducts()
				.OrderBy(p => p.ProductID)
				.Skip((productPage - 1) * _pageSize)
				.Take(_pageSize)
				.ToList();

			var productListViewModel = new ProductListViewModel
			{
				Products = productsList,
				PagingInfo = new PagingInfoViewModel
				{
					CurrentPage = productPage,
					ItemsPerPage = _pageSize,
					TotalItems = _productsRepository.GetProducts().Count()
				}
			};

			return View(productListViewModel);
		}

		public IActionResult Add()
		{
			ViewData["Title"] = "Add Product";

			return View("Edit", new ProductEditViewModel());
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Add(ProductEditViewModel productEditViewModel)
		{
			if (ModelState.IsValid)
			{
				_productsRepository.AddProduct(productEditViewModel.Product);

				return RedirectToAction("Index");
			}
			else
			{
				return View("Edit", productEditViewModel);
			}
		}

		public IActionResult Edit(int id)
		{
			var product = _productsRepository.GetProductById(id);
			if (product == null)
			{
				return NotFound();
			}

			ViewData["Title"] = "Edit Product";

			// Create view model
			var viewModel = new ProductEditViewModel
			{
				Product = product,
				Categories = _categoriesRepository.GetCategories().ToList()
			};

			return View("Edit", viewModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Edit(ProductEditViewModel productEditViewModel)
		{
			if (ModelState.IsValid)
			{
				_productsRepository.UpdateProduct(productEditViewModel.Product);
				return RedirectToAction("Index");
			}

			return View("Edit", productEditViewModel);
		}

		public IActionResult Delete(int id)
		{
			_productsRepository.DeleteProductById(id);

			return RedirectToAction("Index");
		}
	}
}