﻿using AutoMapper;
using CoreNorthWind.Domain.Models;
using CoreNorthWind.Domain.Repositories;
using CoreNorthWind.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace CoreNorthWind.Controllers
{
	[Route("api/categories")]
	[ApiController]
	public class CategoriesAPIController : ControllerBase
	{
		#region Private Fields

		private readonly ICategoriesRepository _categoriesRepository;
		private readonly IMapper _mapper;

		#endregion

		#region Constructor

		public CategoriesAPIController(ICategoriesRepository categoriesRepository, IMapper mapper)
		{
			_categoriesRepository = categoriesRepository;
			_mapper = mapper;
		}

		#endregion


		// GET: api/categories
		[HttpGet]
		public IEnumerable<CategoryAPIModel> Get()
		{
			var categoriesList = _categoriesRepository.GetCategories().ToList();

			var apiModels = _mapper.Map<List<Category>, List<CategoryAPIModel>>(categoriesList);

			return apiModels;
		}
		
		// GET: api/categories/{id}/image
		[HttpGet("{id}/image")]
		public FileResult GetImage(int id)
		{
			var image = _categoriesRepository.GetPicture(id);

			return File(image, "image/bmp");
		}

		// POST: api/categories/{id}/image
		[HttpPost("{id}/image")]
		public void SetImage(int id, IFormFile uploadedFile)
		{
			byte[] imageData = null;

			using (var binaryReader = new BinaryReader(uploadedFile.OpenReadStream()))
			{
				imageData = binaryReader.ReadBytes((int)uploadedFile.Length);
			}

			_categoriesRepository.SetPicture(id, imageData);
		}
	}
}
