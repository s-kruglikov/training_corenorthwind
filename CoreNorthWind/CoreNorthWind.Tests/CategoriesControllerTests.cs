using CoreNorthWind.Controllers;
using CoreNorthWind.Domain.Repositories;
using CoreNorthWind.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace CoreNorthWind.Tests
{
	public class CategoriesControllerTests
	{
		[Fact]
		public void TestGetCategories()
		{
			// Arrange
			var mockRepository = new MockCategoriesRepository();

			var controller = new CategoriesController(mockRepository);

			// Act
			var result = controller.Index();

			// Assert
			var viewResult = Assert.IsType<ViewResult>(result);
			var viewData =
				Assert.IsAssignableFrom<IEnumerable<CategoryListViewModel>>(viewResult.ViewData.Model);
			
			Assert.Equal(5, viewData.Count());
		}
	}
}
