﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreNorthWind.GeneratedClient
{
	[TestClass]
	public class CategoriesApiUnitTest
	{
		[TestMethod]
		public void TestGetAllCategories()
		{
			var api = new CoreNorthWindApi {BaseUri = new Uri(@"https://localhost:5001/")};

			var result = api.CategoriesAPI.GETWithHttpMessagesAsync();
			
			Assert.IsNotNull(result.Result.Body);
			Assert.IsTrue(result.Result.Body.Count > 0);
		}
	}
}