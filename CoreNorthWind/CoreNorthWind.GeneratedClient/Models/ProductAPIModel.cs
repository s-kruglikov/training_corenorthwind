// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace CoreNorthWind.GeneratedClient.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class ProductAPIModel
    {
        /// <summary>
        /// Initializes a new instance of the ProductAPIModel class.
        /// </summary>
        public ProductAPIModel()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ProductAPIModel class.
        /// </summary>
        public ProductAPIModel(string productID = default(string), string productName = default(string), string categoryName = default(string), string unitPrice = default(string), string unitsInStock = default(string), string unitsOnOrder = default(string))
        {
            ProductID = productID;
            ProductName = productName;
            CategoryName = categoryName;
            UnitPrice = unitPrice;
            UnitsInStock = unitsInStock;
            UnitsOnOrder = unitsOnOrder;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "productID")]
        public string ProductID { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "productName")]
        public string ProductName { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "categoryName")]
        public string CategoryName { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "unitPrice")]
        public string UnitPrice { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "unitsInStock")]
        public string UnitsInStock { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "unitsOnOrder")]
        public string UnitsOnOrder { get; set; }

    }
}
