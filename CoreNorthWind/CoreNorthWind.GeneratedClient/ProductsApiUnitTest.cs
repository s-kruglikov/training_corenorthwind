using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreNorthWind.GeneratedClient
{
	[TestClass]
	public class ProductsApiUnitTest
	{
		[TestMethod]
		public void TestGetProduct()
		{
			var api = new CoreNorthWindApi {BaseUri = new Uri(@"https://localhost:5001/")};

			var result = api.ProductsAPI.GET1WithHttpMessagesAsync(1);

			Assert.AreEqual("Chai", result.Result.Body.ProductName);
		}

		[TestMethod]
		public void TestGetAllProducts()
		{
			var api = new CoreNorthWindApi {BaseUri = new Uri(@"https://localhost:5001/")};

			var result = api.ProductsAPI.GETWithHttpMessagesAsync();
			
			Assert.IsNotNull(result.Result.Body);
			Assert.IsTrue(result.Result.Body.Count > 0);
		}
	}
}