﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace CoreNorthWind.ConsoleClient
{
	public class CategoryModel
	{
		[JsonProperty(PropertyName = "categoryID")]
		public string CategoryID { get; set; }

		[JsonProperty(PropertyName = "categoryName")]
		public string CategoryName { get; set; }

		[JsonProperty(PropertyName = "description")]
		public string Description { get; set; }
	}
}
