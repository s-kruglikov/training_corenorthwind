﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreNorthWind.ConsoleClient
{
	public class ProductModel
	{
		public string ProductID { get; set; }

		public string ProductName { get; set; }

		public string CategoryName { get; set; }

		public string UnitPrice { get; set; }

		public string UnitsInStock { get; set; }

		public string UnitsOnOrder { get; set; }
	}
}
