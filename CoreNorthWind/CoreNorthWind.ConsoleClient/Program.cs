﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace CoreNorthWind.ConsoleClient
{
	class Program
	{
		private static readonly HttpClient Client = new HttpClient();

		static void Main()
		{
			RunAsync().GetAwaiter().GetResult();
		}

		private static void ShowCategory(CategoryModel category)
		{
			Console.WriteLine(
				$"Id: {category.CategoryID}\tName: {category.CategoryName}\tDescription: {category.Description}");
		}

		private static void ShowProduct(ProductModel product)
		{
			Console.WriteLine(
				$"Id: {product.ProductID}\tName: {product.ProductName}\tCategory: {product.CategoryName}\r\n" +
				$"Unit Price: {product.UnitPrice}\tUnits In Stock: {product.UnitsInStock}\tUnits On Order: {product.UnitsOnOrder}");
		}

		static async Task RunAsync()
		{
			// Update port # in the following line.
			Client.BaseAddress = new Uri("https://localhost:5001/");
			Client.DefaultRequestHeaders.Accept.Clear();
			Client.DefaultRequestHeaders.Accept.Add(
				new MediaTypeWithQualityHeaderValue("application/json"));

			try
			{
				// Get categories
				var categories = await GetCategoriesAsync("api/categories");

				Console.WriteLine("Categories:");

				foreach (var category in categories)
				{
					ShowCategory(category);
				}

				// Get products
				var products = await GetProductsAsync("api/products");

				Console.WriteLine("Products:");

				foreach (var product in products)
				{
					ShowProduct(product);
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}

			Console.ReadLine();
		}

		static async Task<List<CategoryModel>> GetCategoriesAsync(string path)
		{
			List<CategoryModel> categories = null;

			HttpResponseMessage response = await Client.GetAsync(path);

			if (response.IsSuccessStatusCode)
			{
				categories = await response.Content.ReadAsAsync<List<CategoryModel>>();
			}

			return categories;
		}

		static async Task<List<ProductModel>> GetProductsAsync(string path)
		{
			List<ProductModel> products = null;

			HttpResponseMessage response = await Client.GetAsync(path);

			if (response.IsSuccessStatusCode)
			{
				products = await response.Content.ReadAsAsync<List<ProductModel>>();
			}

			return products;
		}
	}
}
