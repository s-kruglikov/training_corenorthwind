﻿using System;
using System.Linq;

namespace CoreNorthWind.Domain.Utilities
{
	public class ImagesProcessor : IImagesProcessor
	{
		#region Constants

		private const int Offset = 78;

		#endregion

		#region IImagesProcessor Implementations

		/// <summary>
		/// Trims offset from the image bytes array.
		/// </summary>
		/// <param name="picture"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		public byte[] DecodeImage(byte[] picture)
		{
			if (picture == null)
				throw new ArgumentNullException(nameof(picture));

			var processedPicture = new byte[picture.Length - Offset];

			long index = 0;

			foreach (var b in picture.Skip(Offset))
			{
				processedPicture[index] = b;
				index++;
			}

			return processedPicture;
		}

		/// <summary>
		/// Appends offset to the image bytes array.
		/// </summary>
		/// <param name="picture"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		public byte[] EncodeImage(byte[] picture)
		{
			if (picture == null)
				throw new ArgumentNullException(nameof(picture));

			var processedPicture = new byte[picture.Length + Offset];

			long index = Offset;

			for (var i = 0; i < Offset; i++)
			{
				processedPicture[i] = byte.MinValue;
			}

			foreach (var b in picture)
			{
				processedPicture[index] = b;
				index++;
			}

			return processedPicture;
		}

		#endregion
	}
}