﻿namespace CoreNorthWind.Domain.Utilities
{
    /// <summary>
    /// Contains operations for images processing.
    /// </summary>
    public interface IImagesProcessor
    {
        /// <summary>
        /// Processes images retrieved from DB.
        /// </summary>
        /// <param name="picture">Source image stored in DB.</param>
        /// <returns>Processed image.</returns>
        byte[] DecodeImage(byte[] picture);

        /// <summary>
        /// Process images for storing into DB with offset.
        /// </summary>
        /// <param name="picture">Source picture.</param>
        /// <returns>Picture converted for storing in the DB.</returns>
        byte[] EncodeImage(byte[] picture);
    }
}