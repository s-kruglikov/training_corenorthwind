﻿using CoreNorthWind.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace CoreNorthWind.Domain.Contexts
{
	public class DataDbContext : DbContext
	{
		public DataDbContext(DbContextOptions<DataDbContext> options)
			: base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Product>()
				.HasOne(p => p.Category)
				.WithMany(c => c.Products)
				.HasForeignKey(p => p.CategoryID)
				.HasPrincipalKey(c => c.CategoryID);

			modelBuilder.Entity<Product>()
				.HasOne(p => p.Supplier)
				.WithMany(s => s.Products)
				.HasForeignKey(p => p.SupplierID)
				.HasPrincipalKey(s => s.SupplierID);
		}

		public DbSet<Category> Categories { get; set; }

		public DbSet<Product> Products { get; set; }

		public DbSet<Supplier> Suppliers { get; set; }

		
	}
}
