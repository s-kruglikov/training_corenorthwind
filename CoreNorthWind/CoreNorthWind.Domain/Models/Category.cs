﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CoreNorthWind.Domain.Models
{
	public class Category
	{
		public int? CategoryID { get; set; }

		[DisplayName("Name")]
		[Required(ErrorMessage = "Please enter category name")]
		public string CategoryName { get; set; }

		[Required(ErrorMessage = "Please enter category description")]
		public string Description { get; set; }

		public byte[] Picture { get; set; }

		public ICollection<Product> Products { get; set; }
	}
}
