﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace CoreNorthWind.Domain.Models
{
	public class Product
	{
		public int ProductID { get; set; }

		[Required(ErrorMessage = "Please enter product name")]
		public string ProductName { get; set; }

		public int SupplierID { get; set; }

		public Supplier Supplier { get; set; }

		public int CategoryID { get; set; }

		public Category Category { get; set; }

		public string QuantityPerUnit { get; set; }

		public decimal UnitPrice { get; set; }

		[Required(ErrorMessage = "Please enter Units In Stock value.")]
		[Range(1, 1000)]
		public short UnitsInStock { get; set; }

		public short UnitsOnOrder { get; set; }

		public short ReorderLevel { get; set; }

		public bool Discontinued { get; set; }
	}
}
