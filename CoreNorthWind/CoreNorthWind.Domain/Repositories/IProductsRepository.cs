﻿using System.Collections.Generic;
using System.Linq;
using CoreNorthWind.Domain.Models;

namespace CoreNorthWind.Domain.Repositories
{
	public interface IProductsRepository
	{
		IQueryable<Product> GetProducts();

		Product GetProductById(int id);

		void AddProduct(Product product);

		void UpdateProduct(Product product);

		void DeleteProductById(int id);
	}
}
