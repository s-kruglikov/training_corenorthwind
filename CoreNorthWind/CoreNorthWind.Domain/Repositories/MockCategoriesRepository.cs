﻿using System.Collections.Generic;
using System.Linq;
using CoreNorthWind.Domain.Models;

namespace CoreNorthWind.Domain.Repositories
{
	public class MockCategoriesRepository : ICategoriesRepository
	{
		private List<Category> _categoriesList = new List<Category>
		{
			new Category { CategoryName = "category 1"},
			new Category { CategoryName = "category 2"},
			new Category { CategoryName = "category 3"},
			new Category { CategoryName = "category 4"},
			new Category { CategoryName = "category 5"},
		};

		public IQueryable<Category> GetCategories()
		{
			return _categoriesList.AsQueryable();
		}

		public void SaveCategory(Category category)
		{
			throw new System.NotImplementedException();
		}

		public void DeleteCategory(Category category)
		{
			throw new System.NotImplementedException();
		}

		public void AddCategory(Category category)
		{
			_categoriesList.Add(category);
		}

		public void UpdateCategory(Category category)
		{
			throw new System.NotImplementedException();
		}

		public void DeleteCategoryById(int categoryId)
		{
			throw new System.NotImplementedException();
		}

		public byte[] GetPicture(int categoryId)
		{
			return null;
		}

		public void SetPicture(int categoryId, byte[] picture)
		{
			throw new System.NotImplementedException();
		}
	}
}
