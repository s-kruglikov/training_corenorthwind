﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreNorthWind.Domain.Models;

namespace CoreNorthWind.Domain.Repositories
{
	public class MockProductsRepository : IProductsRepository
	{
		public IQueryable<Product> GetProducts()
		{
			throw new NotImplementedException();
		}

		public Product GetProductById(int id)
		{
			throw new NotImplementedException();
		}

		public void AddProduct(Product product)
		{
			throw new NotImplementedException();
		}

		public void UpdateProduct(Product product)
		{
			throw new NotImplementedException();
		}

		public void DeleteProductById(int id)
		{
			throw new NotImplementedException();
		}
	}
}
