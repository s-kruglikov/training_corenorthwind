﻿using System;
using System.Linq;
using CoreNorthWind.Domain.Contexts;
using CoreNorthWind.Domain.Models;
using CoreNorthWind.Domain.Utilities;

namespace CoreNorthWind.Domain.Repositories
{
	public class CategoriesRepository : ICategoriesRepository
	{
		#region Private Fields
		
		private readonly DataDbContext _context;
		private readonly IImagesProcessor _imagesProcessor;
		
		#endregion

		#region Constructor
		
		public CategoriesRepository(DataDbContext context, IImagesProcessor imagesProcessor)
		{
			_context = context;
			_imagesProcessor = imagesProcessor;
		}
		
		#endregion

		public IQueryable<Category> GetCategories()
		{
			return _context.Categories;
		}

		public void SaveCategory(Category category)
		{
			if (category.Picture != null)
			{
				category.Picture = _imagesProcessor.EncodeImage(category.Picture);
			}

			if (category.CategoryID == null)
			{
				AddCategory(category);
			}

			else
			{
				var existingCategory = GetCategories().FirstOrDefault(c => c.CategoryID == category.CategoryID);

				if (existingCategory != null)
				{
					existingCategory.CategoryName = category.CategoryName;
					existingCategory.Description = category.Description;
					existingCategory.Products = category.Products;
					existingCategory.Picture = category.Picture;

					_context.Categories.Update(existingCategory);
				}
			}
			
			_context.SaveChanges();
		}

		public void DeleteCategory(Category category)
		{
			_context.Categories.Remove(category);
			_context.SaveChanges();
		}
		
		public byte[] GetPicture(int categoryId)
		{
			var picture = _context.Categories.FirstOrDefault(c => c.CategoryID == categoryId)?.Picture;

			if (picture != null)
			{
				return _imagesProcessor.DecodeImage(picture);
			}
			else
				throw new ApplicationException("Cannot find picture by provided Category ID.");
		}

		public void SetPicture(int categoryId, byte[] picture)
		{
			var existingCategory = GetCategories().FirstOrDefault(c => c.CategoryID == categoryId);
			
			if (existingCategory != null)
			{
				existingCategory.Picture = _imagesProcessor.EncodeImage(picture);

				_context.Categories.Update(existingCategory);
				_context.SaveChanges();
			}
		}

		#region Internal Implementations
		
		private void AddCategory(Category category)
		{
			_context.Categories.Add(category);
			_context.SaveChanges();
		}
		
		#endregion
	}
}
