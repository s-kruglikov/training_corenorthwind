﻿using System.Linq;
using CoreNorthWind.Domain.Models;

namespace CoreNorthWind.Domain.Repositories
{
	public interface ICategoriesRepository
	{
		IQueryable<Category> GetCategories();

		void SaveCategory(Category category);

		void DeleteCategory(Category category);

		byte[] GetPicture(int categoryId);

		void SetPicture(int categoryId, byte[] picture);
	}
}
