﻿using CoreNorthWind.Domain.Contexts;
using CoreNorthWind.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CoreNorthWind.Domain.Repositories
{
	public class ProductsRepository : IProductsRepository
	{
		private readonly DataDbContext _context;

		public ProductsRepository(DataDbContext context)
		{
			_context = context;
		}

		public IQueryable<Product> GetProducts() => 
			_context.Products
				.Include(p => p.Category)
				.Include(p => p.Supplier);

		public Product GetProductById(int id)
		{
			return GetProducts().SingleOrDefault(p => p.ProductID == id);
		}

		public void AddProduct(Product product)
		{
			_context.Products.Add(product);
			_context.SaveChanges();
		}

		public void UpdateProduct(Product product)
		{
			_context.Products.Update(product);
			_context.SaveChanges();
		}

		public void DeleteProductById(int id)
		{
			_context.Products.Remove(GetProductById(id));

			_context.SaveChanges();
		}
	}
}
